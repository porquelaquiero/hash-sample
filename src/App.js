import React, { Component } from "react";
import "./App.css";
import DirectAddressTable from "./components/DirectAddressTable";
import DoubleHashing from "./components/Double Hashing";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="App">
        <div>
          <DoubleHashing />
        </div>
      </div>
    );
  }
}

export default App;
