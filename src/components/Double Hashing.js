import React, { Component } from "react";

class DoubleHashing extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    var myTableSize = 23; // 테이블 사이즈가 소수여야 효과가 좋다
    const myHashTable = [];

    const getSaveHash = (value) => value % myTableSize;

    const getStepHash = (value) => 17 - (value % 17); // 스텝 해시에 사용되는 수는 테이블 사이즈보다 약간 작은 소수를 사용한다.

    const setValue = (value) => {
      let index = getSaveHash(value);
      let targetValue = myHashTable[index];
      while (true) {
        if (!targetValue) {
          myHashTable[index] = value;
          myTableSize = myHashTable.length >= myTableSize ? myTableSize * 2 : myTableSize;
          return <div>{index}번 인덱스에 {value} 저장!</div>
        } else if (myHashTable.length >= myTableSize) {
        //console.log(myHashTable.length, myHashTable);
        //return <div>{} 풀방입니다</div>;
        return;
        } else {
          //alert(index+"번 인덱스에 "+value+" 저장하려다 충돌 발생!ㅜㅜ");
          index += getStepHash(value);
          index = index > myTableSize ? index - myTableSize : index;
          targetValue = myHashTable[index];  
        }
      }
    };
    return (
      <div className="DoubleHashing">
        <div>{setValue(1)}</div>
        <div>{setValue(2)}</div>
        <div>{setValue(5)}</div>
        <div>{setValue(8)}</div>
        <div>{setValue(11)}</div>
        <div>{setValue(12)}</div>
        <div>{setValue(15)}</div>
        <div>{setValue(18)}</div>
        <div>{setValue(21)}</div>
        <div>{setValue(22)}</div>
        <div>{setValue(24)}</div>
      </div>
    );
  }
}
export default DoubleHashing;
